# Hello world

Example of a Python package stored on the Gitlab local PyPi registry.

## Installing the package

You will have to get your `<personal_token>` from the gitlab settings

```bash
# optionally create a virtualenv/conda env 
conda create -n hello-world python=3 && conda activate hello-world

pip install jelles-hello-world --extra-index-url https://__token__:<your_personal_token>@git.embl.de/api/v4/projects/3732/packages/pypi/simple
```

## Trying it out

After successfull install you can run `hello` to see that it works.


# Hosting your own package

Please be aware that your package name doesn't clash with any existing public package as they can then be installed instead of your own. See also: https://medium.com/@alex.birsan/dependency-confusion-4a5d60fec610

