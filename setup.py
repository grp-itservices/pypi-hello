import os

from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="jelles-hello-world",
    version="0.0.7",
    author="jelle",
    author_email="jelle.scholtalbers@embl.de",
    description=("A simple hello world example"),
    license="BSD",
    keywords="example documentation tutorial pypi registry",
    url="https://git.embl.de/scholtal/",
    scripts=['hello'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
